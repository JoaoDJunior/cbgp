import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { ContatoComponent } from './contato/contato.component';
import { TreinamentosComponent } from './treinamentos/treinamentos.component';
import { SobreComponent } from './sobre/sobre.component';
import { EventosComponent } from './eventos/eventos.component';

const routes: Routes = [
    { path: 'home',             component: HomeComponent },
    { path: 'regras',          component: ContatoComponent },
    { path: 'calendario',     component: TreinamentosComponent },
    { path: 'praticar',            component: SobreComponent },
    { path: 'eventos',     component: EventosComponent },
    // { path: 'grapper',           component: SignupComponent },
    // { path: 'noticias',          component: LandingComponent },
    { path: 'nucleoicons',      component: NucleoiconsComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
