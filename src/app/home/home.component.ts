import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
    model = {
        left: true,
        middle: false,
        right: false
    };

    user: any = {
        email: '',
        senha: '',
    }

    focus;
    focus1;
    constructor(public afAuth: AngularFireAuth) { }

    ngOnInit() {}

    registrar(type, email?, password?) {
        switch (type) {
            case 'google':
                this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
                break;
            case 'facebook':
                this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider);
                break;
            case 'tweeter':
                this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider);
                break;
            default:
                this.afAuth.auth.createUserWithEmailAndPassword(email, password);
                break;
        }
    }
}
